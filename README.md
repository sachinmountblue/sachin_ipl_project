# Sachin_IPL_Project

IPL Data visualisation

#1.Find the number of times each team won the toss and also won the match

#2.Find a player who has won the highest number of Player of the Match awards for each season

#3.Matches Won by each team at each season of IPL

#4.Find the highest number of times one player has been dismissed by another player

#5.Find the bowler with the best economy in super overs

#6.Number of matches played per year for all the years in IPL.

#7.Top 10 economical bowlers in the year 2015.

#8.Extra runs conceded per team in the year 2016

#9.StrikeRate of given batsman at each season
