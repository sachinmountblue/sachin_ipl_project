function fetchAndVisualizeData() {
    fetch("http://localhost:8000/matchPlayedPerYear")
        .then(function (data) {
            return data.json()
        })
        .then(function (jsonData) {
            visualizeMatchesPlayedPerYear(jsonData.matchesPlayedEachYear)
        });

    fetch("http://localhost:8000/matchesWonByTeamPerYear")
        .then(function (data) {
            return data.json()
        })
        .then(function (jsonData) {
            visualizeMatchWonByTeamPerYear(jsonData.matchesWonByTeamEachSeason)

        })

    fetch("http://localhost:8000/ExtraRunConcededPerTeam2016")
        .then(function (data) {
            return data.json()
        })
        .then(function (jsonData) {
            visualizeExtraRunConcededPerTeam2016(jsonData.extraRunsConceded2016)

        })

    fetch("http://localhost:8000/top10EconomicalBowler2015")
        .then(function (data) {
            return data.json()
        })
        .then(function (jsonData) {
            visualizeTop10EconomicalBowler2015(jsonData.top10EconomicalBowler2015)

        })
}

fetchAndVisualizeData();

/**
 *This Function visualize Matches Played Per Year 
 * @param {Object} matchesPlayedEachYear 
 */
function visualizeMatchesPlayedPerYear(matchesPlayedEachYear) {

    Highcharts.chart("matchesPlayedPerYear", {
        chart: {
            type: "column"
        },
        title: {
            text: "Matches Played Per Year"
        },
        subtitle: {
            text: 'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>'
        },
        xAxis: {
            type: "category",
            title: {
                text: "Season"
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: "Matches"
            }
        },
        series: [{
            name: "Matches",
            data: Object.entries(matchesPlayedEachYear)
        }]
    });
}
/**
 * This function visualize Matches Won By Teams Per Year
 * @param {Object} matchesWonByTeamEachSeason 
 */
function visualizeMatchWonByTeamPerYear(matchesWonByTeamEachSeason) {
    let matchData = Object.entries(matchesWonByTeamEachSeason)

    Highcharts.chart('matchesWonByTeamPerYear', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Matches Won By Teams Per Year'
        },
        xAxis: {
            title: {
                text: 'Season'
            },
            categories: ['2008', '2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Matches'
            }
        },
        subtitle: {
            text: 'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>'
        },
        legend: {
            reversed: true
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        series: [{
                name: 'Kolkata Knight Riders',
                data: matchData.map(function (season) {
                    if (season[1]["Kolkata Knight Riders"] == undefined) {
                        return 0
                    } else {
                        return season[1]["Kolkata Knight Riders"]
                    }
                })
            }, {
                name: 'Chennai Super Kings',
                data: matchData.map(function (season) {
                    if (season[1]["Chennai Super Kings"] == undefined) {
                        return 0
                    } else {
                        return season[1]["Chennai Super Kings"]
                    }

                })
            },
            {
                name: 'Delhi Daredevils',
                data: matchData.map(function (season) {
                    if (season[1]["Delhi Daredevils"] == undefined) {
                        return 0
                    } else {
                        return season[1]["Delhi Daredevils"]
                    }
                })
            },
            {
                name: 'Royal Challengers Bangalore',
                data: matchData.map(function (season) {
                    if (season[1]["Royal Challengers Bangalore"] == undefined) {
                        return 0
                    } else {
                        return season[1]["Royal Challengers Bangalore"]
                    }
                })
            }, {
                name: 'Rajasthan Royals',
                data: matchData.map(function (season) {
                    if (season[1]["Rajasthan Royals"] == undefined) {
                        return 0
                    } else {
                        return season[1]["Rajasthan Royals"]
                    }
                })
            }, {
                name: 'Kings XI Punjab',
                data: matchData.map(function (season) {
                    if (season[1]["Kings XI Punjab"] == undefined) {
                        return 0
                    } else {
                        return season[1]["Kings XI Punjab"]
                    }
                })
            }, {
                name: 'Deccan Chargers',
                data: matchData.map(function (season) {
                    if (season[1]["Deccan Chargers"] == undefined) {
                        return 0
                    } else {
                        return season[1]["Deccan Chargers"]
                    }
                })
            }, {
                name: 'Mumbai Indians',
                data: matchData.map(function (season) {
                    if (season[1]["Mumbai Indians"] == undefined) {
                        return 0
                    } else {
                        return season[1]["Mumbai Indians"]
                    }
                })
            },
            {
                name: 'Pune Warriors',
                data: matchData.map(function (season) {
                    if (season[1]["Pune Warriors"] == undefined) {
                        return 0
                    } else {
                        return season[1]["Pune Warriors"]
                    }
                })
            },
            {
                name: 'Kochi Tuskers Kerala',
                data: matchData.map(function (season) {
                    if (season[1]["Kochi Tuskers Kerala"] == undefined) {
                        return 0
                    } else {
                        return season[1]["Kochi Tuskers Kerala"]
                    }
                })
            }, {
                name: 'Sunrisers Hyderabad',
                data: matchData.map(function (season) {
                    if (season[1]["Sunrisers Hyderabad"] == undefined) {
                        return 0
                    } else {
                        return season[1]["Sunrisers Hyderabad"]
                    }
                })
            }, {
                name: 'Gujarat Lions',
                data: matchData.map(function (season) {
                    if (season[1]["Gujarat Lions"] == undefined) {
                        return 0
                    } else {
                        return season[1]["Gujarat Lions"]
                    }
                })
            }, {
                name: 'Rising Pune Supergiants',
                data: matchData.map(function (season) {
                    if (season[1]["Rising Pune Supergiants"] == undefined) {
                        return 0
                    } else {
                        return season[1]["Rising Pune Supergiants"]
                    }
                })
            }

        ]
    });


}
/**
 * This function Visualize Extra Run Conceded Per Team In 2016
 * @param {Object} extraRunsConceded2016 
 */
function visualizeExtraRunConcededPerTeam2016(extraRunsConceded2016) {
    let teamData = Object.entries(extraRunsConceded2016)

    Highcharts.chart("ExtraRunConcededPerTeam2016", {
        chart: {
            type: "column"
        },
        title: {
            text: "Extra Run Conceded Per Team In 2016"
        },
        subtitle: {
            text: 'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>'
        },
        xAxis: {
            type: "category",
            min: 0,
            title: {
                text: "Teams"
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: "Extra Run Conceded"
            }
        },
        series: [{
            name: "Extra Run",
            data: teamData.map((team) => {
                return [team[0], team[1].extraRuns]
            })
        }]
    });

}
/**
 * This function visualize Top 10 Economical Blower In 2015
 * @param {Object} top10EconomicalBowler2015 
 */
function visualizeTop10EconomicalBowler2015(top10EconomicalBowler2015) {

    Highcharts.chart('top10EconomicalBowler2015', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Top 10 Economical Bowler In 2015'
        },
        subtitle: {
            text: 'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>'
        },
        xAxis: {
            type: "category",
            title: {
                text: 'Player'
            }

        },
        yAxis: {
            min: 0,
            title: {
                text: 'Bowler Economy'
            }
        },
        legend: {
            enabled: false
        },
        series: [{
            name: 'Economy',
            data: top10EconomicalBowler2015

        }]
    });


}