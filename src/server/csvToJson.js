const fs = require('fs')
const csv = require('csvtojson')

const matchesCsvFilePath = "../data/matches.csv"
const deliveriesCsvFilePath = "../data/deliveries.csv"

const matchesJsonFilePath = "../data/matches.json"
const deliveriesJsonFilePath = "../data/deliveries.json"

/**
 * Convert csv file to json
 * @param {String} csvFilePath 
 * @param {String} jsonFilePath 
 */
function csvToJson(csvFilePath, jsonFilePath) {

    csv()
        .fromFile(csvFilePath)
        .then((jsonData) => {
            const jsonString = JSON.stringify(jsonData)
            fs.writeFile(jsonFilePath, jsonString, err => {
                if (err) {
                    console.error(err)
                }
            })
        })

}
csvToJson(matchesCsvFilePath, matchesJsonFilePath)
csvToJson(deliveriesCsvFilePath, deliveriesJsonFilePath)