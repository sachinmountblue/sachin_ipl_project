const fs = require('fs')

function createOutputJsonFile(result, fileName, objectName) {
    const jsonData = {
        [objectName]: result
    }
    const jsonString = JSON.stringify(jsonData)

    fs.writeFile(fileName, jsonString, err => {
        if (err) {
            console.error(err)
        }
    })
}

module.exports = createOutputJsonFile