const fs = require('fs')
const matches = require('./ipl.js')
const outputJsonFile = require('./createOutputJsonFile.js')

const matchesJsonFilePath = "../data/matches.json"
const deliveriesJsonFilePath = "../data/deliveries.json"

const matchesData = JSON.parse(fs.readFileSync(matchesJsonFilePath));
const deliveriesData = JSON.parse(fs.readFileSync(deliveriesJsonFilePath));


let matchesPlayedEachYear = matches.matchesPlayedEachYear
let wonMatchAndTossTeam = matches.wonMatchAndTossTeam
let playerOfMatch = matches.playerOfMatch
let matchesWonByTeamEachSeason = matches.matchesWonByTeamEachSeason
let highestTimeDismissed = matches.highestTimeDismissed
let bestEconomyInSuperOver = matches.bowlerWithBestEconomyInSuperOver
let top10EconomicalBowler2015 = matches.top10EconomicalBowler2015
let extraRunsConceded2016 = matches.extraRunsConceded2016
let strikeRateOfBatsmanEachYear = matches.strikeRateOfBatsmanEachYear
let manOfMatchRun2010 = matches.manOfMatchRun2010

let resultMatchesPlayedEachYear = matchesPlayedEachYear(matchesData)
let resultWonMatchAndTossTeam = wonMatchAndTossTeam(matchesData)
let resultPlayerOfMatch = playerOfMatch(matchesData)
let resultMatchesWonByTeamEachSeason = matchesWonByTeamEachSeason(matchesData)
let resultHighestTimeDismissed = highestTimeDismissed(deliveriesData)
let resultBestEconomyInSuperOver = bestEconomyInSuperOver(deliveriesData)
let resultTop10EconomicalBowler2015 = top10EconomicalBowler2015(matchesData, deliveriesData)
let resultExtraRunsConceded2016 = extraRunsConceded2016(matchesData, deliveriesData)
let resultStrikeRateOfBatsmanEachYear = strikeRateOfBatsmanEachYear(matchesData, deliveriesData, "V Kohli")
let resultManOfMatchRun2010 = manOfMatchRun2010(matchesData, deliveriesData)

outputJsonFile(resultMatchesPlayedEachYear, "../public/output/matchesPerYear.json", "matchesPlayedEachYear")
outputJsonFile(resultWonMatchAndTossTeam, "../public/output/wonTossAndMatches.json", "wonTossAndMatches")
outputJsonFile(resultPlayerOfMatch, "../public/output/playerOfMatch.json", "playerOfMatch")
outputJsonFile(resultMatchesWonByTeamEachSeason, "../public/output/matchesWonByTeamEachSeason.json", "matchesWonByTeamEachSeason")
outputJsonFile(resultHighestTimeDismissed, "../public/output/highestTimeDismissed.json", "highestTimeDismissed")
outputJsonFile(resultBestEconomyInSuperOver, "../public/output/bestEconomyInSuperOver.json", "bestEconomyInSuperOver")
outputJsonFile(resultTop10EconomicalBowler2015, "../public/output/top10EconomicalBowler2015.json", "top10EconomicalBowler2015")
outputJsonFile(resultExtraRunsConceded2016, "../public/output/extraRunsConceded2016.json", "extraRunsConceded2016")
outputJsonFile(resultStrikeRateOfBatsmanEachYear, "../public/output/strikeRateOfBatsman.json", "strikeRateOfBatsman")