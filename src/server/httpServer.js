const http = require('http')
const fs = require('fs')
const url = require('url')
const path = require('path')
const port = 8000
const host = 'localhost'

const server = http.createServer((req, res) => {
    let serverUrl = url.parse(req.url)

    switch (serverUrl.pathname) {
        case "/index.html":
            fs.readFile(path.join(__dirname, "../public/index.html"), (err, data) => {
                if (err) {
                    res.writeHead(404)
                    res.end(JSON.stringify({
                        error: `${http.STATUS_CODES[404]}`,
                        errorMessage: `${err}`
                    }))
                } else {
                    res.writeHead(200)
                    res.end(data)

                }

            })
            break

        case "/matchPlayedPerYear":
            fs.readFile(path.join(__dirname, "../public/output/matchesPerYear.json"), (err, data) => {
                if (err) {
                    res.writeHead(404)
                    res.end(JSON.stringify({
                        error: `${http.STATUS_CODES[404]}`,
                        errorMessage: `${err}`
                    }))
                } else {
                    res.writeHead(200)
                    res.end(data)
                }
            })
            break
        case "/matchesWonByTeamPerYear":
            fs.readFile(path.join(__dirname, "../public/output/matchesWonByTeamEachSeason.json"), (err, data) => {
                if (err) {
                    res.writeHead(404)
                    res.end(JSON.stringify({
                        error: `${http.STATUS_CODES[404]}`,
                        errorMessage: `${err}`
                    }))

                } else {
                    res.writeHead(200)
                    res.end(data)
                }

            })
            break
        case "/ExtraRunConcededPerTeam2016":
            fs.readFile(path.join(__dirname, "../public/output/extraRunsConceded2016.json"), (err, data) => {
                if (err) {
                    res.writeHead(404)
                    res.end(JSON.stringify({
                        error: `${http.STATUS_CODES[404]}`,
                        errorMessage: `${err}`
                    }))
                } else {
                    res.writeHead(200)
                    res.end(data)
                }
            })
            break
        case "/top10EconomicalBowler2015":
            fs.readFile(path.join(__dirname, "../public/output/top10EconomicalBowler2015.json"), (err, data) => {
                if (err) {
                    res.writeHead(404)
                    res.end(JSON.stringify({
                        error: `${http.STATUS_CODES[404]}`,
                        errorMessage: `${err}`
                    }))
                } else {
                    res.writeHead(200)
                    res.end(data)
                }

            })
            break

        case "/app.js":
            fs.readFile(path.join(__dirname, "../public/app.js"), (err, data) => {
                if (err) {
                    res.writeHead(404)
                    res.end(JSON.stringify({
                        error: `${http.STATUS_CODES[404]}`,
                        errorMessage: `${err}`
                    }))
                } else {
                    res.writeHead(200)
                    res.end(data)
                }
            })
            break
        default:
            res.writeHead(404)
            res.end(JSON.stringify({
                error: `${http.STATUS_CODES[404]}`
            }))


    }


})

server.listen(port, host, () => {
    console.log(`Server is running on http://${host}:${port}`)
}).on('error', (e) => {
    console.error(e.message)
    throw e

})