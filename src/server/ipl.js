//@ts-check
/**
 * This function accepts data of all the IPL matches in the form of object and
 * return the object contains the data of Number of matches played in each season of IPL 
 * 
 * @param {object} matches data of all the IPL matches
 * @returns{object} Number of matches played in each season of IPL
 */
function matchesPlayedEachYear(matches) {
    let matchesPlayedEachYear = matches.reduce((seasonObj, match) => {
        if (seasonObj[match.season] !== undefined) {
            seasonObj[match.season] += 1
        } else {
            seasonObj[match.season] = {}
            seasonObj[match.season] = 1
        }
        return seasonObj
    }, {})
    return matchesPlayedEachYear
}

/**
 * This function accepts data of all the IPL matches in the form of object and return 
 * the object,which contains the data  about the number of times each team won the toss 
 * and also won the match
 * 
 * @param {object} matches data of all the IPL matches
 * @returns {object}  number of times each team won the toss and also won the match
 */
function wonMatchAndTossTeam(matches) {
    let wonMatchAndTossTeam = matches.reduce((teamsNameObj, match) => {
        if (match.toss_winner === match.winner) {
            if (teamsNameObj[match.winner] !== undefined) {
                teamsNameObj[match.winner] += 1
            } else {
                teamsNameObj[match.winner] = 1
            }
        }
        return teamsNameObj
    }, {})
    return wonMatchAndTossTeam
}
/**
 * This function accepts data of all the IPL matches in the form of object and return
 * the object, which contains the player who has won the highest number of Player of 
 * the Match awards for each season
 * 
 * @param {object} matches data of all the IPL matches
 * @returns {object}player who has won the highest number of Player of the Match awards for each season
 */
function playerOfMatch(matches) {
    let playerOfMatch = matches.reduce((iplSeasonObj, match) => {
        if (iplSeasonObj[match.season]) {
            if (iplSeasonObj[match.season][match.player_of_match] !== undefined) {
                iplSeasonObj[match.season][match.player_of_match] += 1
            } else {
                iplSeasonObj[match.season][match.player_of_match] = 1
            }
        } else {
            iplSeasonObj[match.season] = {}
            iplSeasonObj[match.season][match.player_of_match] = 1
        }
        return iplSeasonObj
    }, {})

    let playerOfMatchEachSeason = Object.keys(playerOfMatch).reduce((iplSeasonObj, season) => {
        iplSeasonObj[season] = Object.entries(playerOfMatch[season]).
        sort(function (a, b) {
                return b[1] - a[1]
            })
            .slice(0, 1)
        return iplSeasonObj
    }, {})
    return playerOfMatchEachSeason
}

/**
 *This function accepts data of all the IPL matches in the form of object and return
 *the object, which contains Number Matches Won by Each team in every season of IPL

 * @param {object} matches data of each match played in all season of IPL
 * @returns {object} Number Matches Won by Each team in every season of IPL

 */
function matchesWonByTeamEachSeason(matches) {
    let matchesWonByTeamEachSeason = matches.reduce((iplSeasonObj, match) => {
        if (iplSeasonObj[match.season]) {
            if (iplSeasonObj[match.season][match.winner] !== undefined) {
                iplSeasonObj[match.season][match.winner] += 1
            } else {
                iplSeasonObj[match.season][match.winner] = 1
            }
        } else {
            iplSeasonObj[match.season] = {}
            iplSeasonObj[match.season][match.winner] = 1
        }
        return iplSeasonObj
    }, {})
    return matchesWonByTeamEachSeason
}

/**
 * This function accepts data of each and every ball delivered in all the matches of IPL and return
 *the object, which contains highest number of times one player has been dismissed by another player
 *
 * @param {object} deliveries data of each and every ball delivered in all the matches of IPL
 * @returns {object}highest number of times one player has been dismissed by another player
 */
function highestTimeDismissed(deliveries) {
    let timeDismissed = deliveries.reduce((playerObj, delivery) => {
        if (delivery.batsman == delivery.player_dismissed && delivery.dismissal_kind != 'run out') {
            let batsmanBowlerString = delivery.batsman.concat(" dismissed by ", delivery.bowler)
            if (playerObj[batsmanBowlerString] !== undefined) {
                playerObj[batsmanBowlerString] += 1
            } else {
                playerObj[batsmanBowlerString] = 1
            }
        }
        return playerObj
    }, {})
    let highestTimeDismissed = Object.entries(timeDismissed).
    sort(function (a, b) {
        return b[1] - a[1]
    }).
    slice(0, 1)

    return {
        highestTimeDismissed
    }

}

/**
 * This function accepts data of each and every ball delivered in all the matches of IPL and return
 *the object, which contains the bowler with the best economy in super overs
 *
 * @param {object} deliveries data of each and every ball delivered in all the matches of IPL
 * @returns {object}the bowler with the best economy in super overs
 */
function bowlerWithBestEconomyInSuperOver(deliveries) {
    let bowlerInSuperOver = deliveries.reduce((bowlerObj, delivery) => {
        if (delivery.is_super_over > 0) {
            let runGiven = parseInt(delivery.total_runs) - (parseInt(delivery.bye_runs) + parseInt(delivery.legbye_runs))
            if (bowlerObj[delivery.bowler] !== undefined) {
                bowlerObj[delivery.bowler]["Run"] += (runGiven)
                bowlerObj[delivery.bowler]["Balls"] += 1
                bowlerObj[delivery.bowler]["Economy"] = parseFloat((bowlerObj[delivery.bowler].Run / (bowlerObj[delivery.bowler].Balls / 6)).toPrecision(3));
            } else {
                bowlerObj[delivery.bowler] = {}
                bowlerObj[delivery.bowler]["Run"] = (runGiven)
                bowlerObj[delivery.bowler]["Balls"] = 1
            }
        }
        return bowlerObj

    }, {})
    let bestBowlerInSuperOver = Object.keys(bowlerInSuperOver).map(element => {
        return [element, bowlerInSuperOver[element].Economy]
    }).sort(function (a, b) {
        return a[1] - b[1]
    }).slice(0, 1)

    return {
        bestBowlerInSuperOver
    }
}

/**
 * This function takes data of each deliveries of every match in form of deliveries object and return Top 10 
 * Economical bowler of season 2015.
 * @param {Object} deliveries data of each deliveries of every match played in IPL all season
 * @returns {object} Top 10 economical bowler of year 2015
 */

function top10EconomicalBowler2015(matchesData, deliveries) {
    let matchesPlayed2015 = matchesData.reduce((matchArr, match) => {
        if (match.season == 2015) {
            matchArr.push(match.id)
        }
        return matchArr

    }, [])
    let firstMatch = parseInt(matchesPlayed2015[0])
    let lastMatch = parseInt(matchesPlayed2015[matchesPlayed2015.length - 1])
    let bowlers = deliveries.filter(eachDelivery => {
        if (eachDelivery["match_id"] >= firstMatch && eachDelivery["match_id"] <= lastMatch) {
            return eachDelivery
        }
    }).reduce((accumulateObj, delivery) => {
            if (accumulateObj[delivery.bowler] === undefined) {
                accumulateObj[delivery.bowler] = {
                    balls: 0,
                    runs: 0,
                };
            }
            accumulateObj[delivery.bowler].balls += 1;
            accumulateObj[delivery.bowler].runs += parseInt(delivery.total_runs) - parseInt(delivery.bye_runs) - parseInt(delivery.legbye_runs);
            return accumulateObj;
        }, {}

    );
    const economicalBowlers = Object.keys(bowlers).reduce((accumulateObj, bowler) => {
        accumulateObj[bowler] = parseFloat((bowlers[bowler].runs / (bowlers[bowler].balls / 6)).toPrecision(3));
        return accumulateObj;
    }, {});

    let top10Economical = Object.entries(economicalBowlers)
        .sort((a, b) => a[1] - b[1])
        .slice(0, 10)
    return top10Economical
}
/**
 * This function take data of all the ball delivered in all season of ipl and return the extra runs 
 * conceded by each teams in year 2016
 * @param {object} deliveries data of all the ball delivered in all season of ipl
 * @returns {object} extraRuns2016 extra runs conceded by each teams in year 2016
 */

function extraRunsConceded2016(MatchesData, deliveries) {
    let matchesPlayed2016 = MatchesData.reduce((matchArr, match) => {
        if (match.season == 2016) {
            matchArr.push(match.id)
        }
        return matchArr

    }, [])
    let firstMatch = parseInt(matchesPlayed2016[0])
    let lastMatch = parseInt(matchesPlayed2016[matchesPlayed2016.length - 1])

    let extraRuns2016 = deliveries.reduce((accumulateObj, delivery) => {
        if (parseInt(delivery.match_id) >= firstMatch && parseInt(delivery.match_id) <= lastMatch) {

            if (accumulateObj[delivery.bowling_team] === undefined) {
                accumulateObj[delivery.bowling_team] = {
                    extraRuns: 0
                }
            }
            accumulateObj[delivery.bowling_team].extraRuns += parseInt(delivery.extra_runs)

        }
        return accumulateObj
    }, {})

    return extraRuns2016
}
/**
 * This function accept deliveries object and Player Name and return the object of Run scored by the given Batsman at each season
 * 
 * @param {Object} deliveries Data about every ball delivered in IPL all season
 * @param {string} name Player Name
 * @returns Run scored by the given Batsman at each season
 */

function strikeRateOfBatsmanEachYear(matchesData, deliveries, name) {
    let seasonAndMatchData = matchesData.reduce((seasonAndMatchData, match) => {
        seasonAndMatchData[match.id] = match.season;
        return seasonAndMatchData;
    }, {});

    let strikeRateOfBatsman = deliveries.filter(delivery => {
        if (delivery.batsman === name) {
            return delivery
        }
    }).reduce((strikeRate, delivery) => {
        if (strikeRate[seasonAndMatchData[delivery.match_id]] === undefined) {
            strikeRate[seasonAndMatchData[delivery.match_id]] = {
                run: parseInt(delivery.batsman_runs),
                ball: 1
            }
        } else {
            strikeRate[seasonAndMatchData[delivery.match_id]].run += parseInt(delivery.batsman_runs)
            strikeRate[seasonAndMatchData[delivery.match_id]].ball += 1

            strikeRate[seasonAndMatchData[delivery.match_id]].strikeRate = (
                (strikeRate[seasonAndMatchData[delivery.match_id]].run *
                    100) /
                strikeRate[seasonAndMatchData[delivery.match_id]].ball
            ).toFixed(2);
        }
        return strikeRate;
    }, {})

    return {
        [name]: strikeRateOfBatsman

    }
}

function manOfMatchRun2010(matchesData, deliveriesData) {

    let seasonAndMatchData = matchesData.reduce((seasonAndMatchData, match) => {
        if (match.season == "2010") {
            seasonAndMatchData[match.id] = match.player_of_match;
        }
        return seasonAndMatchData

    }, {})
    let matchIdArr = Object.keys(seasonAndMatchData)
    let firstMatch = parseInt(matchIdArr[0])
    let lastMatch = parseInt(matchIdArr[matchIdArr.length - 1])

    let runScoredByManOfMatch = deliveriesData.filter((delivery) => {
        if (delivery.match_id >= firstMatch && delivery.match_id <= lastMatch) {
            return delivery
        }
    }).reduce((playerRun, delivery) => {


        if (playerRun[seasonAndMatchData[delivery.match_id]] === undefined) {
            playerRun[seasonAndMatchData[delivery.match_id]] = {
                run: 0
            }
        }
        if (seasonAndMatchData[delivery.match_id] == delivery.batsman) {

            playerRun[seasonAndMatchData[delivery.match_id]].run += parseInt(delivery.batsman_runs)
        }
        return playerRun

    }, {})

    return runScoredByManOfMatch

}


module.exports = {
    matchesPlayedEachYear,
    wonMatchAndTossTeam,
    playerOfMatch,
    matchesWonByTeamEachSeason,
    highestTimeDismissed,
    bowlerWithBestEconomyInSuperOver,
    top10EconomicalBowler2015,
    extraRunsConceded2016,
    strikeRateOfBatsmanEachYear,
    manOfMatchRun2010
}